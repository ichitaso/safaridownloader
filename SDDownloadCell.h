@interface SDDownloadCell : UITableViewCell
@property(nonatomic,readonly) UIProgressView* progressView;
@property(nonatomic,readonly) UILabel* rightTextLabel;
-(id)initWithReuseIdentifier:(NSString*)identifier;
@end
