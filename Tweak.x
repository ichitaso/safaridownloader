#import <MobileCoreServices/UTType.h>
#import <WebKit/WebKit.h>
#import <dispatch/dispatch.h>
#import <objc/runtime.h>
#include "SDDownloadCell.h"
#include "SDRotatingViewController.h"
#include "libfinder/LFClient.h"
#include "libfinder/LFExtras.h"
#include "libfinder/LFFinderController.h"
#include "libfinder/LFTemporaryFile.h"

#define CONFIG_PLIST @"/var/mobile/Library/Preferences/com.officialscheduler.safaridownloader.plist"
#define TMP_DIR @"/tmp/SDECache"

#define REQ_FORCE_DL @"SDForceDownload"
#define REQ_PASSTHRU @"SDPassthru"
#define DL_CHANGED @"SDDownloadDidChange"
#define DL_COUNT_CHANGED @"SDDownloadCountDidChange"
#define ATYPE_OPEN 1
#define ATYPE_CONFIRM 2

@interface NSURLDownload : NSObject
-(void)cancel;
-(void)setDestination:(NSString*)path allowOverwrite:(BOOL)overwrite;
@end

@interface NSURLRequest (Private)
-(id)_propertyForKey:(id)key;
-(void)_removePropertyForKey:(id)key;
-(void)_setProperty:(id)property forKey:(id)key;
@end

@interface UIViewController (Private)
-(UIPopoverController*)_popoverController;
@end

@interface UIWebElementAction : NSObject
-(id)initWithTitle:(NSString*)title actionHandler:(void(^)(id,NSURL*,id))handler type:(int)type;
-(NSString*)_title;
@end

@interface BrowserController : NSObject
+(BrowserController*)sharedBrowserController;
-(id)browserView;
-(id)buttonBar;
-(void)_presentModalViewController:(UIViewController*)controller fromRectInToolbar:(CGRect)rect;
-(void)_presentModalViewController:(UIViewController*)controller fromButtonIdentifier:(int)identifier;
-(void)setShowingReader:(BOOL)show animated:(BOOL)animated;
@end

@interface YTVideoReference
-(NSDictionary*)archiveDictionary;
-(NSURL*)contentURL;
-(int)profile;
@end

@interface EXTERNAL
-(CGRect)bookmarksPopoverPresentationRect;
-(NSBundle*)bundle;
-(void)download;
-(WebView*)webView;
@end

static NSDictionary* $_config=nil;
static CFMutableDictionaryRef $_dMap;

static void $_showMessage(NSString* msg) {
  static UIView* messageView=nil;
  static UILabel* labelView=nil;
  if(!messageView){
    messageView=[[UIView alloc] init];
    messageView.autoresizingMask=UIViewAutoresizingFlexibleLeftMargin
     |UIViewAutoresizingFlexibleRightMargin
     |UIViewAutoresizingFlexibleTopMargin
     |UIViewAutoresizingFlexibleBottomMargin;
    messageView.backgroundColor=[UIColor colorWithWhite:0 alpha:0.8];
    messageView.layer.cornerRadius=10;
    messageView.alpha=0;
    labelView=[[UILabel alloc] init];
    labelView.font=[UIFont boldSystemFontOfSize:20];
    labelView.textColor=[UIColor whiteColor];
    labelView.backgroundColor=[UIColor clearColor];
    [messageView addSubview:labelView];
    static UIImage* iconImage=nil;
    if(!iconImage){
      iconImage=[[UIImage alloc] initWithContentsOfFile:
       @"/Library/PreferenceLoader/Preferences/com.officialscheduler.safaridownloader/icon.png"];
    }
    UIImageView* imageView=[[UIImageView alloc] initWithImage:iconImage];
    imageView.center=(CGPoint){25,25};
    [messageView addSubview:imageView];
    [imageView release];
    [[[UIApplication sharedApplication].delegate.window.subviews
     objectAtIndex:0] addSubview:messageView];
  }
  CGSize csize=messageView.superview.bounds.size,
   lsize=[msg sizeWithFont:labelView.font],vsize={lsize.width+65,50};
  messageView.frame=(CGRect){{(csize.width-vsize.width)/2,(csize.height-vsize.height)/2},vsize};
  labelView.frame=(CGRect){{50,(vsize.height-lsize.height)/2},lsize};
  labelView.text=msg;
  [UIView animateWithDuration:0.2 animations:^{
    messageView.alpha=1;
  } completion:^(BOOL finished){
    if(finished){
      [UIView animateWithDuration:0.5 delay:1.5
       options:UIViewAnimationOptionCurveEaseInOut animations:^{
        messageView.alpha=0;
      } completion:^(BOOL finished){
        if(finished){
          [messageView removeFromSuperview];
          [messageView release];messageView=nil;
          [labelView release];labelView=nil;
        }
      }];
    }
  }];
}
static NSMutableURLRequest* $_requestWithURL(NSURL* URL,NSURL* referer) {
  NSMutableURLRequest* request=[NSMutableURLRequest requestWithURL:URL];
  [request addValue:referer.absoluteString forHTTPHeaderField:@"Referer"];
  return request;
}
static void $_dismissAndLoadFileURL(UIViewController* controller,NSURL* URL) {
  UIPopoverController* popover=controller._popoverController;
  if(popover){[popover dismissPopoverAnimated:NO];}
  else {[controller dismissViewControllerAnimated:NO completion:NULL];}
  [[%c(BrowserController) sharedBrowserController].browserView
   loadRequest:[%c(SDFileURLProtocol) canonicalRequestForRequest:
   [NSURLRequest requestWithURL:URL]]];
}
static void $_startDownload(NSURLRequest* request) {
  [request _setProperty:[NSNumber numberWithBool:YES] forKey:REQ_FORCE_DL];
  BrowserController* browser=[%c(BrowserController) sharedBrowserController];
  [browser setShowingReader:NO animated:YES];
  [browser.browserView loadRequest:request];
}
static void $_loadConfig(CFNotificationCenterRef center,void* observer,CFStringRef name,const void* object,CFDictionaryRef userInfo) {
  if($_config){[$_config release];}
  $_config=[[NSDictionary alloc] initWithContentsOfFile:CONFIG_PLIST];
}
static BOOL $_boolForKey(NSString* key) {
  NSString* option=[$_config objectForKey:key];
  return option?option.boolValue:YES;
}
static BOOL $_isSpecialURL(NSURL* URL) {
  return [URL.scheme isEqualToString:@"ftp"] && !URL.host;
}
static id $_getIdentifier(NSURL* URL) {
  id identifier;
  [URL getResourceValue:&identifier
   forKey:NSURLFileResourceIdentifierKey error:NULL];
  return identifier;
}

@interface SDDownloadInfo : NSObject {
  id identifier;
  @public
  BOOL canceled;
  time_t startTime;
  size_t receivedBytes,expectedBytes;
}
@end
@implementation SDDownloadInfo
-(BOOL)matchesURL:(NSURL*)URL {
  return [$_getIdentifier(URL) isEqual:identifier];
}
-(void)download:(NSURLDownload*)download didCreateDestination:(NSString*)path {
  [identifier release];
  if([identifier=$_getIdentifier([NSURL fileURLWithPath:path]) retain]){
    CFDictionaryAddValue($_dMap,identifier,download);
    [[NSNotificationCenter defaultCenter]
     postNotificationName:DL_COUNT_CHANGED object:nil];
    NSString* msg=NSLocalizedStringWithDefaultValue(@"DOWNLOADING_STATUS",@"Delayed",
     [NSBundle bundleWithIdentifier:@"com.apple.Message"],@"\0",nil);
    if(![msg characterAtIndex:0]){
      msg=NSLocalizedStringFromTableInBundle(@"Downloading",@"Progress",
       [NSBundle bundleWithIdentifier:@"com.apple.Foundation"],nil);
    }
    CFIndex count=CFDictionaryGetCount($_dMap);
    $_showMessage((count>1)?[msg stringByAppendingFormat:@" (%ld)",count]:msg);
  }
}
-(void)dealloc {
  if(identifier){
    CFDictionaryRemoveValue($_dMap,identifier);
    [identifier release];
    [[NSNotificationCenter defaultCenter]
     postNotificationName:DL_COUNT_CHANGED object:nil];
    if(!canceled && !CFDictionaryGetCount($_dMap)){
      $_showMessage(NSLocalizedStringFromTableInBundle(@"Done",nil,
       [NSBundle bundleWithIdentifier:@"com.apple.UIKit"],nil));
    }
  }
  [super dealloc];
}
@end

@interface SDDownloadsController : UITableViewController <LFFinderActionDelegate,UIActionSheetDelegate> {
  dispatch_queue_t queue;
  NSMutableArray* downloads;
  UIBarButtonItem* clearItem,*saveItem;
  UIDocumentInteractionController* DIC;
  NSURL* targetURL;
  UIView* targetView;
}
@end
@implementation SDDownloadsController
+(void)downloadDidBegin:(NSURLDownload*)download {
  SDDownloadInfo* info=[[SDDownloadInfo alloc] init];
  objc_setAssociatedObject(download,$_dMap,info,OBJC_ASSOCIATION_RETAIN_NONATOMIC);
  [info release];
} 
+(void)download:(NSURLDownload*)download decideDestinationWithSuggestedFilename:(NSString*)name {
  [[NSFileManager defaultManager] createDirectoryAtPath:TMP_DIR
   withIntermediateDirectories:YES attributes:nil error:NULL];
  [download setDestination:[TMP_DIR stringByAppendingPathComponent:[name
   stringByReplacingOccurrencesOfString:@"/" withString:@"%"]]
   allowOverwrite:NO];
}
+(void)download:(NSURLDownload*)download didCreateDestination:(NSString*)path {
  objc_msgSend(objc_getAssociatedObject(download,$_dMap),_cmd,download,path);
}
+(void)download:(NSURLDownload*)download didReceiveResponse:(NSURLResponse*)response {
  SDDownloadInfo* info=objc_getAssociatedObject(download,$_dMap);
  time(&info->startTime);
  info->receivedBytes=0;
  info->expectedBytes=response.expectedContentLength;
  [[NSNotificationCenter defaultCenter]
   postNotificationName:DL_CHANGED object:download];
}
+(void)download:(NSURLDownload*)download didReceiveDataOfLength:(NSUInteger)length {
  SDDownloadInfo* info=objc_getAssociatedObject(download,$_dMap);
  info->receivedBytes+=length;
  [[NSNotificationCenter defaultCenter]
   postNotificationName:DL_CHANGED object:download];
}
+(void)downloadDidFinish:(NSURLDownload*)download {
  [[NSNotificationCenter defaultCenter]
   postNotificationName:DL_CHANGED object:download
   userInfo:[NSDictionary dictionary]];
}
+(void)download:(NSURLDownload*)download didFailWithError:(NSError*)error {
  [error showAlert];
  SDDownloadInfo* info=objc_getAssociatedObject(download,$_dMap);
  info->canceled=YES;
  [[NSNotificationCenter defaultCenter]
   postNotificationName:DL_CHANGED object:download
   userInfo:[NSDictionary dictionary]];
}
-(id)init {
  if((self=[super initWithStyle:UITableViewStylePlain])){
    queue=dispatch_queue_create(NULL,NULL);
    downloads=[[NSMutableArray alloc] init];
    self.title=NSLocalizedStringFromTableInBundle(@"Downloads",nil,
     [NSBundle bundleWithIdentifier:@"com.apple.UIKit"],nil);
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(updateDownloadCount)
     name:DL_COUNT_CHANGED object:nil];
  }
  return self;
}
-(void)dismiss {
  UIPopoverController* popover=self._popoverController;
  if(popover){[popover dismissPopoverAnimated:YES];}
  else {[self dismissViewControllerAnimated:YES completion:NULL];}
}
-(void)updateDownloadCount {
  dispatch_sync(queue,^{
    BOOL enabled=NO;
    for (NSURL* URL in downloads){
      if(!CFDictionaryGetValue($_dMap,$_getIdentifier(URL))){
        enabled=YES;
        break;
      }
    }
    clearItem.enabled=saveItem.enabled=enabled;
  });
}
-(void)updateCell:(SDDownloadCell*)cell URL:(NSURL*)URL download:(NSURLDownload*)download {
  cell.textLabel.text=URL.lastPathComponent;
  UIProgressView* progressView=cell.progressView;
  if((progressView.hidden=!download)){
    NSDate* fileDate;
    [URL getResourceValue:&fileDate forKey:NSURLContentModificationDateKey error:NULL];
    cell.detailTextLabel.text=[NSDateFormatter localizedStringFromDate:fileDate
     dateStyle:NSDateFormatterMediumStyle timeStyle:NSDateFormatterShortStyle]?:@"---";
    NSNumber* fileSize;
    [URL getResourceValue:&fileSize forKey:NSURLFileSizeKey error:NULL];
    cell.rightTextLabel.text=fileSize.stringByFormattingAsFileSize;
  }
  else {
    SDDownloadInfo* info=objc_getAssociatedObject(download,$_dMap);
    cell.detailTextLabel.text=[NSString localizedStringWithFormat:@"%@ / %@ (%.1f%%)",
     [NSNumber numberWithUnsignedLongLong:info->receivedBytes].stringByFormattingAsFileSize,
     [NSNumber numberWithUnsignedLongLong:info->expectedBytes].stringByFormattingAsFileSize,
     (progressView.progress=(float)info->receivedBytes/info->expectedBytes)*100];
    time_t currentTime;
    time(&currentTime);
    cell.rightTextLabel.text=[[NSNumber numberWithDouble:info->receivedBytes
     /difftime(currentTime,info->startTime)].stringByFormattingAsFileSize
     stringByAppendingString:@"/s"];
  }
}
-(void)downloadDidChange:(NSNotification*)note {
  dispatch_sync(queue,^{
    NSURLDownload* download=note.object;
    SDDownloadInfo* info=objc_getAssociatedObject(download,$_dMap);
    UITableView* view=self.tableView;
    for (NSIndexPath* ipath in view.indexPathsForVisibleRows){
      NSURL* URL=[downloads objectAtIndex:ipath.row];
      if([info matchesURL:URL]){
        [self updateCell:(SDDownloadCell*)[view cellForRowAtIndexPath:ipath]
         URL:URL download:note.userInfo?nil:download];
      }
    }
  });
}
-(void)pushFinder:(LFFinderController*)finder {
  UINavigationController* nav=self.navigationController;
  UIViewController* master=nav.parentViewController;
  [master addChildViewController:finder];
  CGRect frame=nav.view.frame,leftFrame=frame,rightFrame=frame;
  leftFrame.origin.x-=leftFrame.size.width;
  rightFrame.origin.x+=rightFrame.size.width;
  finder.view.frame=rightFrame;
  UISwipeGestureRecognizer* gesture=[[UISwipeGestureRecognizer alloc]
   initWithTarget:self action:@selector(popFinder)];
  gesture.direction=UISwipeGestureRecognizerDirectionRight;
  [finder.navigationBar addGestureRecognizer:gesture];
  [gesture release];
  [master transitionFromViewController:nav toViewController:finder
   duration:master.view.window?0.25:0 options:0 animations:^{
    finder.view.frame=frame;
    nav.view.frame=leftFrame;
  } completion:^(BOOL finished){
    [finder didMoveToParentViewController:master];
  }];
}
-(void)pushFinder {
  LFFinderController* finder=[[LFFinderController alloc] init];
  finder.actionDelegate=self;
  [self pushFinder:finder];
  [finder release];
}
-(void)popFinder {
  UINavigationController* nav=self.navigationController;
  UIViewController* master=nav.parentViewController;
  LFFinderController* finder=master.childViewControllers.lastObject;
  [finder willMoveToParentViewController:nil];
  CGRect frame=finder.view.frame,leftFrame=frame,rightFrame=frame;
  leftFrame.origin.x-=leftFrame.size.width;
  rightFrame.origin.x+=rightFrame.size.width;
  nav.view.frame=leftFrame;
  [master transitionFromViewController:finder toViewController:nav
   duration:0.25 options:0 animations:^{
    nav.view.frame=frame;
    finder.view.frame=rightFrame;
  } completion:^(BOOL finished){
    [finder removeFromParentViewController];
  }];
}
-(void)clearHistory {
  UIActionSheet* sheet=[[UIActionSheet alloc]
   initWithTitle:nil delegate:self
   cancelButtonTitle:NSLocalizedString(@"Cancel (action sheet)",nil)
   destructiveButtonTitle:NSLocalizedString(@"Clear History (alert sheet bookmarks)",nil)
   otherButtonTitles:nil];
  sheet.tag=ATYPE_CONFIRM;
  [sheet showFromBarButtonItem:clearItem animated:YES];
  [sheet release];
}
-(void)saveAll {
  LFFinderController* finder=[[LFFinderController alloc] init];
  finder.sourcePath=TMP_DIR;
  finder.actionDelegate=self;
  [self pushFinder:finder];
  [finder release];
}
-(void)donate {
  [self dismiss];
  [[UIApplication sharedApplication] openURL:
   [NSURL URLWithString:@"https://www.paypal.com/cgi-bin/webscr"
   "?cmd=_s-xclick&hosted_button_id=SAZC3J3V76Z76"]];
}
-(void)finder:(LFFinderController*)finder didSelectItemAtPath:(NSString*)path {
  $_dismissAndLoadFileURL(finder,[NSURL fileURLWithPath:path]);
}
-(void)finder:(LFFinderController*)finder didSelectPath:(NSString*)path {
  [finder dismiss];
  dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0),^{
    NSAutoreleasePool* pool=[[NSAutoreleasePool alloc] init];
    for (NSURL* URL in downloads){
      if(CFDictionaryGetValue($_dMap,$_getIdentifier(URL))){continue;}
      NSString* name=URL.lastPathComponent,*base,*ext;
      unsigned long counter=0;
      while(1){
        NSString* dest=[path stringByAppendingPathComponent:name];
        NSError* error;
        if([LFClient moveItemAtPath:URL.path toPath:dest error:&error]){break;}
        if([error.domain isEqualToString:NSPOSIXErrorDomain] && error.code==EEXIST){
          if(!counter){
            ext=name.pathExtension;
            base=ext.length?name.stringByDeletingPathExtension:(ext=nil,name);
          }
          name=[base stringByAppendingFormat:@"_%lu",++counter];
          if(ext){name=[name stringByAppendingPathExtension:ext];}
        }
        else {
          [error showAlert];
          break;
        }
      }
    }
    rmdir(TMP_DIR.fileSystemRepresentation);
    [pool drain];
  });
}
-(void)actionSheet:(UIActionSheet*)sheet clickedButtonAtIndex:(NSInteger)index {
  switch(sheet.tag){
    case ATYPE_OPEN:
      if(index==0){
        $_dismissAndLoadFileURL(self,targetURL);
      }
      else if(index==1){
        LFFinderController* finder=[[LFFinderController alloc]
         initWithMode:LFFinderModeLink];
        finder.sourcePath=targetURL.path;
        [self pushFinder:finder];
        [finder release];
      }
      else if(index==2){
        if(!DIC){DIC=[[UIDocumentInteractionController alloc] init];}
        DIC.URL=targetURL;
        [DIC presentOpenInMenuFromRect:targetView.bounds inView:targetView animated:YES];
      }
      [targetURL release];
      targetURL=nil;
      [targetView release];
      targetView=nil;
      break;
    case ATYPE_CONFIRM:
      if(index==0){
        NSFileManager* manager=[NSFileManager defaultManager];
        NSMutableIndexSet* indexes=[NSMutableIndexSet indexSet];
        NSMutableArray* ipaths=[NSMutableArray array];
        dispatch_sync(queue,^{
          NSUInteger i=0;
          for (NSURL* URL in downloads){
            if(!CFDictionaryGetValue($_dMap,$_getIdentifier(URL))){
              [manager removeItemAtURL:URL error:NULL];
              [indexes addIndex:i];
              [ipaths addObject:[NSIndexPath indexPathForRow:i inSection:0]];
            }
            i++;
          }
          rmdir(TMP_DIR.fileSystemRepresentation);
          [downloads removeObjectsAtIndexes:indexes];
        });
        [self updateDownloadCount];
        [self.tableView deleteRowsAtIndexPaths:ipaths
         withRowAnimation:UITableViewRowAnimationLeft];
      }
      break;
  }
}
-(NSInteger)tableView:(UITableView*)view numberOfRowsInSection:(NSInteger)section {
  return downloads.count;
}
-(UITableViewCell*)tableView:(UITableView*)view cellForRowAtIndexPath:(NSIndexPath*)ipath {
  SDDownloadCell* cell=(SDDownloadCell*)[view dequeueReusableCellWithIdentifier:@"Cell"]?:
   [[[SDDownloadCell alloc] initWithReuseIdentifier:@"Cell"] autorelease];
  NSURL* URL=[downloads objectAtIndex:ipath.row];
  [self updateCell:cell URL:URL download:(NSURLDownload*)CFDictionaryGetValue($_dMap,$_getIdentifier(URL))];
  return cell;
}
-(void)tableView:(UITableView*)view didSelectRowAtIndexPath:(NSIndexPath*)ipath {
  [view deselectRowAtIndexPath:ipath animated:YES];
  NSURL* URL=[downloads objectAtIndex:ipath.row];
  if(!CFDictionaryGetValue($_dMap,$_getIdentifier(URL))){
    targetURL=[URL retain];
    targetView=[[view cellForRowAtIndexPath:ipath] retain];
    NSBundle* bundle=[NSBundle bundleWithIdentifier:@"com.apple.UIKit"];
    UIActionSheet* sheet=[[UIActionSheet alloc]
     initWithTitle:URL.lastPathComponent delegate:self
     cancelButtonTitle:NSLocalizedString(@"Cancel (action sheet)",nil)
     destructiveButtonTitle:NSLocalizedStringFromTableInBundle(@"Select",nil,bundle,nil)
     otherButtonTitles:NSLocalizedStringFromTableInBundle(@"Save",nil,bundle,nil),
      NSLocalizedStringFromTableInBundle(@"Open In...",nil,bundle,nil),nil];
    sheet.tag=ATYPE_OPEN;
    [sheet showFromRect:targetView.bounds inView:targetView animated:YES];
    [sheet release];
  }
}
-(void)tableView:(UITableView*)view commitEditingStyle:(UITableViewCellEditingStyle)style forRowAtIndexPath:(NSIndexPath*)ipath {
  if(style==UITableViewCellEditingStyleDelete){
    NSUInteger index=ipath.row;
    NSURL* URL=[downloads objectAtIndex:index];
    NSNumber* linkCount;
    [URL getResourceValue:&linkCount forKey:NSURLLinkCountKey error:NULL];
    if(linkCount.unsignedIntegerValue==1){
      NSURLDownload* download=(NSURLDownload*)CFDictionaryGetValue($_dMap,$_getIdentifier(URL));
      if(download){
        [SDDownloadsController download:download didFailWithError:nil];
        [download cancel];
      }
    }
    dispatch_sync(queue,^{
      [[NSFileManager defaultManager] removeItemAtURL:URL error:NULL];
      rmdir(TMP_DIR.fileSystemRepresentation);
      [downloads removeObjectAtIndex:index];
    });
    [self updateDownloadCount];
    [view deleteRowsAtIndexPaths:[NSArray arrayWithObject:ipath]
     withRowAnimation:UITableViewRowAnimationLeft];
  }
}
-(void)viewWillDisappear:(BOOL)animated {
  [super viewWillDisappear:animated];
  [[NSNotificationCenter defaultCenter]
   removeObserver:self name:DL_CHANGED object:nil];
}   
-(void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
  [downloads setArray:[[NSFileManager defaultManager]
   contentsOfDirectoryAtURL:[NSURL fileURLWithPath:TMP_DIR]
   includingPropertiesForKeys:[NSArray arrayWithObjects:
   NSURLFileResourceIdentifierKey,NSURLContentModificationDateKey,
   NSURLFileSizeKey,nil] options:0 error:NULL]];
  [downloads sortUsingComparator:(NSComparator)^(NSURL* URL1,NSURL* URL2){
    NSDate* fileDate1,*fileDate2;
    [URL1 getResourceValue:&fileDate1 forKey:NSURLContentModificationDateKey error:NULL];
    [URL2 getResourceValue:&fileDate2 forKey:NSURLContentModificationDateKey error:NULL];
    NSComparisonResult cmp=[fileDate2 compare:fileDate1];
    return (cmp==NSOrderedSame)?[URL1.lastPathComponent
     localizedStandardCompare:URL2.lastPathComponent]:cmp;
  }];
  [self updateDownloadCount];
  [self.tableView reloadData];
  [[NSNotificationCenter defaultCenter]
   addObserver:self selector:@selector(downloadDidChange:)
   name:DL_CHANGED object:nil];
}
-(void)viewDidLoad {
  self.tableView.rowHeight=65;
  UIBarButtonItem* dismissItem=self._popoverController?nil:
   [[UIBarButtonItem alloc] initWithBarButtonSystemItem:
    UIBarButtonSystemItemCancel target:self action:@selector(dismiss)],
   *spaceItem=[[UIBarButtonItem alloc] initWithBarButtonSystemItem:
    UIBarButtonSystemItemFlexibleSpace target:nil action:NULL],
   *donateItem=[[UIBarButtonItem alloc] initWithTitle:@"\U0001F4E6 Donate"
    style:UIBarButtonItemStyleBordered target:self action:@selector(donate)];
  self.toolbarItems=dismissItem?
   [NSArray arrayWithObjects:dismissItem,spaceItem,donateItem,nil]:
   [NSArray arrayWithObjects:spaceItem,donateItem,nil];
  UINavigationItem* navitem=self.navigationItem;
  NSBundle* bundle=[NSBundle bundleWithIdentifier:@"com.apple.UIKit"];
  navitem.leftBarButtonItem=clearItem=[[UIBarButtonItem alloc]
   initWithTitle:NSLocalizedStringFromTableInBundle(@"Clear",nil,bundle,nil)
   style:UIBarButtonItemStyleBordered target:self action:@selector(clearHistory)];
  navitem.rightBarButtonItem=saveItem=[[UIBarButtonItem alloc]
   initWithTitle:NSLocalizedStringFromTableInBundle(@"Save",nil,bundle,nil)
   style:UIBarButtonItemStyleDone target:self action:@selector(saveAll)];
  [dismissItem release];
  [spaceItem release];
  [donateItem release];
}
-(void)dealloc {
  [[NSNotificationCenter defaultCenter] removeObserver:self];
  dispatch_release(queue);
  [downloads release];
  [DIC release];
  [clearItem release];
  [saveItem release];
  [super dealloc];
}
@end

@interface SDLinkActionSheet : UIActionSheet
@property(nonatomic,readonly) NSURLRequest* request;
@end
@implementation SDLinkActionSheet
@synthesize request;
+(void)actionSheet:(SDLinkActionSheet*)sheet clickedButtonAtIndex:(NSInteger)index {
  if(index==0){$_startDownload(sheet.request);}
  else if(index==1){
    [UIPasteboard generalPasteboard].string=sheet.request.URL.absoluteString;
  }
}
-(id)initWithRequest:(NSURLRequest*)_request {
  NSURL* URL=_request.URL;
  NSBundle* bundle=[NSBundle bundleWithIdentifier:@"com.apple.WebCore"];
  if((self=[super initWithTitle:[NSString localizedStringWithFormat:
   @"<%@>\n%@",URL.lastPathComponent,URL.host?:@"---"]
   delegate:(id)[SDLinkActionSheet class]
   cancelButtonTitle:NSLocalizedString(@"Cancel (action sheet)",nil)
   destructiveButtonTitle:nil otherButtonTitles:
   NSLocalizedStringFromTableInBundle(@"Download Linked File",nil,bundle,nil),
   NSLocalizedStringFromTableInBundle(@"Copy Link",nil,bundle,nil),nil])){
    request=[_request retain];
  }
  return self;
}
-(id)initWithFPV:(id)fpv {
  NSURL* URL;
  static Ivar var=NULL;
  if(var){URL=object_getIvar(fpv,var);}
  else {var=object_getInstanceVariable(fpv,"_currentContentURL",(void**)&URL);}
  WebDataSource* source=[(id)[fpv superview] webView].mainFrame.dataSource;
  NSURL* pageURL=source.response.URL;
  return [self initWithRequest:[pageURL isEqual:URL]?
   source.request:$_requestWithURL(URL,pageURL)];
}
-(void)showWithGesture:(UIGestureRecognizer*)gesture {
  UIView* view=gesture.view;
  [self showFromRect:(CGRect){[gesture locationInView:view],{0}}
   inView:view animated:YES];
}
-(void)dealloc {
  [request release];
  [super dealloc];
}
@end

@interface SDYTActionSheet : UIActionSheet
@property(nonatomic,readonly) UIGestureRecognizer* gesture;
@property(nonatomic,retain) NSArray* URLs;
@end
@implementation SDYTActionSheet
@synthesize gesture,URLs;
+(void)actionSheet:(SDYTActionSheet*)sheet clickedButtonAtIndex:(NSInteger)index {
  if(index<sheet.cancelButtonIndex){
    SDLinkActionSheet* sheet2=[[SDLinkActionSheet alloc]
     initWithRequest:[NSURLRequest requestWithURL:[sheet.URLs objectAtIndex:index]]];
    [sheet2 showWithGesture:sheet.gesture];
    [sheet2 release];
  }
}
-(void)showWithGesture:(UIGestureRecognizer*)_gesture {
  if(gesture!=_gesture){
    [gesture release];
    gesture=[_gesture retain];
  }
  UIView* view=gesture.view;
  [self showFromRect:(CGRect){[gesture locationInView:view],{0}}
   inView:view animated:YES];
}
-(void)dealloc {
  [gesture release];
  [URLs release];
  [super dealloc];
}
@end

@interface SDFileURLProtocol : NSURLProtocol {BOOL stopped;} @end
@implementation SDFileURLProtocol
+(BOOL)canInitWithRequest:(NSURLRequest*)request {
  if([request _propertyForKey:REQ_PASSTHRU]){return NO;}
  NSURL* URL=request.URL;
  return URL.isFileURL || $_isSpecialURL(URL);
}
+(NSURLRequest*)canonicalRequestForRequest:(NSURLRequest*)request {
  NSURL* URL=request.URL;
  NSString* path=URL.resourceSpecifier;
  if(URL.host){
    NSRange range=[path rangeOfString:@"^//[^/]*"
     options:NSRegularExpressionSearch];
    if(range.location==0){path=[path substringFromIndex:range.length];}
  }
  NSMutableString* mpath=path.mutableCopy;
  [mpath replaceOccurrencesOfString:@"/{2,}" withString:@"/"
   options:NSRegularExpressionSearch range:NSMakeRange(0,mpath.length)];
  [mpath replaceOccurrencesOfString:@"#" withString:@"%23"
   options:0 range:NSMakeRange(0,mpath.length)];
  [mpath replaceOccurrencesOfString:@"?" withString:@"%3F"
   options:0 range:NSMakeRange(0,mpath.length)];
  path=[@"ftp://" stringByAppendingString:mpath?:@"/"];
  [mpath release];
  return [NSURLRequest requestWithURL:[NSURL URLWithString:path]];
}
-(void)startLoading {
  NSURL* URL=self.request.URL;
  NSString* path=URL.path;
  id client=self.client;
  BOOL isDir;
  if(![LFClient fileExistsAtPath:path isDirectory:&isDir]){
    [client URLProtocol:self didFailWithError:[NSError
     errorWithDomain:NSPOSIXErrorDomain code:ENOENT userInfo:nil]];
    return;
  }
  if(isDir && ![URL.absoluteString hasSuffix:@"/"]){
    NSHTTPURLResponse* res=[[NSHTTPURLResponse alloc] initWithURL:URL
     statusCode:304 HTTPVersion:@"HTTP/1.1" headerFields:nil];
    [client URLProtocol:self wasRedirectedToRequest:[NSURLRequest
     requestWithURL:[NSURL fileURLWithPath:path isDirectory:YES]]
     redirectResponse:res];
    [res release];
    return;
  }
  CFStringRef UTI=UTTypeCreatePreferredIdentifierForTag(
   kUTTagClassFilenameExtension,(CFStringRef)path.pathExtension,NULL);
  CFStringRef MIMEType=UTTypeCopyPreferredTagWithClass(UTI,kUTTagClassMIMEType);
  CFRelease(UTI);
  NSHTTPURLResponse* res=[[NSHTTPURLResponse alloc] initWithURL:URL
   statusCode:200 HTTPVersion:@"HTTP/1.1" headerFields:MIMEType?[NSDictionary
   dictionaryWithObject:(id)MIMEType forKey:@"content-type"]:nil];
  if(MIMEType){CFRelease(MIMEType);}
  [client URLProtocol:self didReceiveResponse:res
   cacheStoragePolicy:NSURLCacheStorageNotAllowed];
  [res release];
  NSError* error;
  if(isDir){
    NSArray* contents=[[LFClient contentsOfDirectoryAtPath:path error:&error]
     sortedArrayUsingSelector:@selector(localizedStandardCompare:)];
    if(contents){
      unsigned long count=contents.count;
      [client URLProtocol:self didLoadData:[[NSString localizedStringWithFormat:
       @"<HTML><HEAD><meta name=viewport content=\"width=device-width\">"
       "<TITLE>%@</TITLE><STYLE>table{font:14px Arial;}a{text-decoration:none;color:black;}"
       "a.dir{font-weight:bold;color:blue;}a.x{color:red;}"
       "a.link{color:cyan;}a.socket{color:magenta;}a.other{color:brown;}"
       "td{white-space:nowrap;}td.date{padding:3px 1em 3px 1em;color:gray;}"
       "</STYLE></HEAD><BODY><h2>%@ <font color=lightgray>(%lu)</font></h2>",
       path.lastPathComponent.stringByEscapingXMLSpecialCharacters,
       path.stringByEscapingXMLSpecialCharacters,count]
       dataUsingEncoding:NSUTF8StringEncoding]];
      const char* str;
      if(count){
        str="<table cellspacing=0 cellpadding=0>";
        [client URLProtocol:self didLoadData:[NSData dataWithBytes:str length:strlen(str)]];
        for (NSString* name in contents){
          if(stopped){return;}
          NSString* _path=[path stringByAppendingPathComponent:name];
          NSDictionary* attributes=[LFClient attributesOfItemAtPath:_path error:NULL];
          BOOL _isDir,exists=[LFClient fileExistsAtPath:_path isDirectory:&_isDir];
          NSString* _type=attributes.fileType;
          [client URLProtocol:self didLoadData:[[NSString localizedStringWithFormat:
           @"<tr><td><a href=\"%@\" class=\"%s %s\">%@</a></td>"
           "<td class=date>%@</td><td align=right>%@ B</td></tr>",
           [name stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],
           exists?_isDir?"dir":"":"x",
           [_type isEqualToString:NSFileTypeRegular]
           || [_type isEqualToString:NSFileTypeDirectory]?"":
           [_type isEqualToString:NSFileTypeSymbolicLink]?"link":
           [_type isEqualToString:NSFileTypeSocket]?"socket":"other",
           name.stringByEscapingNewlines.stringByEscapingXMLSpecialCharacters,
           [NSDateFormatter localizedStringFromDate:attributes.fileModificationDate
           dateStyle:NSDateFormatterShortStyle timeStyle:NSDateFormatterShortStyle]
           .stringByEscapingXMLSpecialCharacters?:@"?",
           [NSNumberFormatter localizedStringFromNumber:[attributes objectForKey:NSFileSize]
           numberStyle:NSNumberFormatterDecimalStyle].stringByEscapingXMLSpecialCharacters?:@"?"]
           dataUsingEncoding:NSUTF8StringEncoding]];
        }
        str="</table>";
        [client URLProtocol:self didLoadData:[NSData dataWithBytes:str length:strlen(str)]];
      }
      str="</BODY></HTML>";
      [client URLProtocol:self didLoadData:[NSData dataWithBytes:str length:strlen(str)]];
      [client URLProtocolDidFinishLoading:self];
    }
    else {[client URLProtocol:self didFailWithError:error];}
  }
  else {
    NSFileHandle* handle=[LFClient openFileAtPath:path mode:"rb" error:&error];
    if(handle){
      while(!stopped){
        NSData* data=[handle readDataOfLength:4096];
        if(data.length){[client URLProtocol:self didLoadData:data];}
        else {
          [client URLProtocolDidFinishLoading:self];
          break;
        }
      }
      [handle release];
    }
    else {[client URLProtocol:self didFailWithError:error];}
  }
}
-(void)stopLoading {
  stopped=YES;
}
@end

%hook UIApplication
-(void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent*)event {
  %orig;
  if($_boolForKey(@"downloadMedia") && motion==UIEventSubtypeMotionShake){
    static ptrdiff_t offset=0;
    if(!offset){
      offset=ivar_getOffset(class_getInstanceVariable(
       %c(FigPluginView),"_havePreparedForPlayback"));
    }
    for (id fpv in [[%c(BrowserController) sharedBrowserController].browserView subviews]){
      if([fpv isKindOfClass:%c(FigPluginView)] && *(BOOL*)((void*)fpv+offset)){
        SDLinkActionSheet* sheet=[[SDLinkActionSheet alloc] initWithFPV:fpv];
        [sheet showInView:[self.delegate.window.subviews objectAtIndex:0]];
        [sheet release];
      }
    }
  }
}
%end

%hook BrowserController
-(BOOL)isAnyPageLoaded {
  return %orig || CFDictionaryGetCount($_dMap);
}
-(NSArray*)_actionsForElement:(id)element withTargetURL:(NSURL*)URL suggestedActions:(NSArray*)suggested {
  NSMutableArray* actions=[NSMutableArray array];
  NSString* s_AddToReadingList=NSLocalizedStringWithDefaultValue(
   @"Add to Reading List",nil,[NSBundle mainBundle],@"\0",nil);
  if(![s_AddToReadingList characterAtIndex:0]){
    s_AddToReadingList=NSLocalizedString(@"Add to Reading List (Long press)",nil);
  }
  NSString* s_SaveImage=NSLocalizedStringFromTableInBundle(@"Save Image",nil,
   [NSBundle bundleWithIdentifier:@"com.apple.UIKit"],nil);
  NSBundle* bundle=[NSBundle bundleWithIdentifier:@"com.apple.WebCore"];
  for (UIWebElementAction* action in %orig){
    NSString* title=action._title;
    if([title isEqualToString:s_AddToReadingList]){
      if($_boolForKey(@"downloadLinks")){
        UIWebElementAction* action=[[UIWebElementAction alloc]
         initWithTitle:NSLocalizedStringFromTableInBundle(@"Download Linked File",nil,bundle,nil)
         actionHandler:^(id node,NSURL* URL,id bview){
         $_startDownload($_requestWithURL(URL,[bview URL]));} type:0];
        [actions addObject:action];
        [action release];
      }
      if(!$_boolForKey(@"showAddToReadingList")){continue;}
    }
    if([title isEqualToString:s_SaveImage]){
      if($_boolForKey(@"downloadImages")){
        UIWebElementAction* action=[[UIWebElementAction alloc]
         initWithTitle:NSLocalizedStringFromTableInBundle(@"Download Image",nil,bundle,nil)
         actionHandler:^(id node,NSURL* URL,id bview){
         $_startDownload($_requestWithURL([node absoluteImageURL],[bview URL]));} type:3];
        [actions addObject:action];
        [action release];
      }
      if(!$_boolForKey(@"showSaveImage")){continue;}
    }
    [actions addObject:action];
  }
  return actions;
}
%end

%hook BrowserToolbar
-(void)_installGestureRecognizers {
  %orig;
  UILongPressGestureRecognizer* gesture=[[UILongPressGestureRecognizer alloc]
   initWithTarget:self action:@selector(SDE_handleShowFinderGesture:)];
  static Ivar var=NULL;
  if(!var){var=class_getInstanceVariable(%c(BrowserToolbar),"_bookmarksItem");}
  [[object_getIvar(self,var) view] addGestureRecognizer:gesture];
  [gesture release];
}
%new
-(void)SDE_handleShowFinderGesture:(UILongPressGestureRecognizer*)gesture {
  if(gesture.state==UIGestureRecognizerStateBegan){
    SDDownloadsController* sub=[[SDDownloadsController alloc] init];
    UINavigationController* nav=[[UINavigationController alloc]
     initWithRootViewController:sub];
    nav.toolbarHidden=NO;
    UIViewController* master=[[SDRotatingViewController alloc] init];
    [master addChildViewController:nav];
    nav.view.frame=master.view.bounds;
    [master.view addSubview:nav.view];
    [nav didMoveToParentViewController:master];
    UISwipeGestureRecognizer* gesture=[[UISwipeGestureRecognizer alloc]
     initWithTarget:sub action:@selector(pushFinder)];
    gesture.direction=UISwipeGestureRecognizerDirectionLeft;
    [nav.navigationBar addGestureRecognizer:gesture];
    [gesture release];
    [nav release];
    if(![[NSFileManager defaultManager] enumeratorAtPath:TMP_DIR].nextObject){[sub pushFinder];}
    [sub release];
    BrowserController* browser=[%c(BrowserController) sharedBrowserController];
    if([browser respondsToSelector:@selector(_presentModalViewController:fromRectInToolbar:)]){
      [browser _presentModalViewController:master fromRectInToolbar:
       [browser.buttonBar bookmarksPopoverPresentationRect]];
    }
    else {[browser _presentModalViewController:master fromButtonIdentifier:4];}
    [master release];
  }
}
%end

%hook TabDocument
-(void)webView:(WebView*)view decidePolicyForMIMEType:(NSString*)MIMEType request:(NSURLRequest*)request frame:(WebFrame*)wframe decisionListener:(id)listener {
  if([request _propertyForKey:REQ_FORCE_DL]){
    [request _removePropertyForKey:REQ_FORCE_DL];
    goto __download;
  }
  else if(!$_isSpecialURL(request.URL) && ![%c(WebView) canShowMIMEType:MIMEType]) __download:{
    view.downloadDelegate=[SDDownloadsController class];
    [listener download];
  }
  else {
    view.downloadDelegate=self;
    %orig;
  }
}
%end

%hook ReaderContext
-(void)webView:(WebView*)view decidePolicyForNavigationAction:(NSDictionary*)action request:(NSURLRequest*)request frame:(WebFrame*)wframe decisionListener:(id)listener {
  [request _setProperty:[NSNumber numberWithBool:YES] forKey:REQ_PASSTHRU];
  %orig;
}
%end

%hook WebHistory
-(void)_visitedURL:(NSURL*)URL withTitle:(NSString*)title method:(NSString*)method wasFailure:(BOOL)failure increaseVisitCount:(BOOL)icount {
  if(!$_isSpecialURL(URL)){%orig;}
}
%end

%group QTPlugin
%hook FigPluginView
+(id)plugInViewWithArguments:(NSMutableDictionary*)arguments {
  NSURL* URL=[arguments valueForKey:@"WebPlugInBaseURLKey"];
  LFTemporaryFile* file=$_isSpecialURL(URL)?
   [[LFTemporaryFile alloc] initWithPath:URL.path forWriting:NO]:nil;
  BOOL cmode=$_boolForKey(@"downloadMedia") && [[arguments
   valueForKey:@"WebPlugInModeKey"] intValue]!=0;
  if(file || cmode){arguments=[NSMutableDictionary dictionaryWithDictionary:arguments];}
  if(file){
    NSMutableDictionary* dict=[[arguments valueForKey:@"WebPlugInAttributesKey"] mutableCopy];
    [dict setValue:[[NSURL fileURLWithPath:file.path] absoluteString] forKey:@"src"];
    [arguments setValue:dict forKey:@"WebPlugInAttributesKey"];
    [arguments setValue:file forKey:@"SDTemporaryFile"];
    [file release];
    [dict release];
  }
  if(cmode){
    [arguments setValue:[NSNumber numberWithInt:0] forKey:@"WebPlugInModeKey"];
  }
  return %orig;
}
-(void)setAllowPlayback:(BOOL)allow {
  %orig;
  if($_boolForKey(@"downloadMedia")){
    UIView* button;
    static Ivar var=NULL;
    if(var){button=object_getIvar(self,var);}
    else {var=object_getInstanceVariable(self,"_playButton",(void**)&button);}
    if(button){
      UILongPressGestureRecognizer* gesture=[[UILongPressGestureRecognizer alloc]
       initWithTarget:self action:@selector(SDE_handleLongPressGesture:)];
      [button addGestureRecognizer:gesture];
      [gesture release];
    }
  }
}
%new
-(void)SDE_handleLongPressGesture:(UILongPressGestureRecognizer*)gesture {
  if(gesture.state==UIGestureRecognizerStateBegan){
    SDLinkActionSheet* sheet=[[SDLinkActionSheet alloc] initWithFPV:self];
    [sheet showWithGesture:gesture];
    [sheet release];
  }
}
%end
%end // QTPlugin

%group YTPlugin
%hook YouTubePlugInView
+(id)plugInViewWithArguments:(id)arguments {
  id view=%orig;
  if($_boolForKey(@"downloadMedia")){
    UIView* button;
    static Ivar var=NULL;
    if(var){button=object_getIvar(view,var);}
    else {var=object_getInstanceVariable(view,"_playButton",(void**)&button);}
    if(button){
      UILongPressGestureRecognizer* gesture=[[UILongPressGestureRecognizer alloc]
       initWithTarget:view action:@selector(SDE_handleLongPressGesture:)];
      [button addGestureRecognizer:gesture];
      [gesture release];
    }
  }
  return view;
}
%new
-(void)SDE_handleLongPressGesture:(UILongPressGestureRecognizer*)gesture {
  if(gesture.state==UIGestureRecognizerStateBegan){
    NSArray* videos;
    static Ivar var1=NULL,var2;
    if(var1){videos=object_getIvar(object_getIvar(self,var2),var1);}
    else {
      var1=object_getInstanceVariable(self,"_video",(void**)&videos);
      var2=object_getInstanceVariable(videos,"_videoReferences",(void**)&videos);
    }
    if(!videos.count){return;}
    SDYTActionSheet* sheet=[[SDYTActionSheet alloc] 
     initWithTitle:NSLocalizedStringFromTableInBundle(@"Choose File",nil,
      [NSBundle bundleWithIdentifier:@"com.apple.WebCore"],nil)
     delegate:(id)[SDYTActionSheet class]
     cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:nil];
    NSMutableArray* URLs=[NSMutableArray array];
    for (YTVideoReference* video in videos){
      unsigned int dS=[[video.archiveDictionary objectForKey:@"duration"] intValue],dM=dS/60,dH=dM/60;
      [sheet addButtonWithTitle:[NSString localizedStringWithFormat:@"%d (%02u:%02u:%02u)",
       video.profile,dH,dM-dH*60,dS-dM*60]];
      [URLs addObject:video.contentURL];
    }
    sheet.URLs=URLs;
    sheet.cancelButtonIndex=[sheet addButtonWithTitle:
     NSLocalizedString(@"Cancel (action sheet)",nil)];
    [sheet showWithGesture:gesture];
    [sheet release];
  }
}
%end
%end // YTPlugin

%hook WebPluginPackage
-(BOOL)load {
  BOOL ok=%orig;
  if(ok){
    NSString* identifier=[self bundle].bundleIdentifier;
    if([identifier isEqualToString:@"com.apple.quicktime.plugin"]){
      static BOOL $_initQTPlugin=YES;
      if($_initQTPlugin){
        $_initQTPlugin=NO;
        %init(QTPlugin);
      }
    }
    else if([identifier isEqualToString:@"com.apple.youtubeplugin"]){
      static BOOL $_initYTPlugin=YES;
      if($_initYTPlugin){
        $_initYTPlugin=NO;
        %init(YTPlugin);
      }
    }
  }
  return ok;
}
%end

%ctor {
  NSAutoreleasePool* pool=[[NSAutoreleasePool alloc] init];
  $_loadConfig(NULL,NULL,NULL,NULL,NULL);
  if($_boolForKey(@"enabled")){
    %init;
    $_dMap=CFDictionaryCreateMutable(NULL,0,&kCFTypeDictionaryKeyCallBacks,NULL);
    [NSURLProtocol registerClass:%c(SDFileURLProtocol)];
    CFNotificationCenterAddObserver(CFNotificationCenterGetDarwinNotifyCenter(),NULL,
     $_loadConfig,CFSTR("com.officialscheduler.safaridownloader.reload"),NULL,
     CFNotificationSuspensionBehaviorCoalesce);
  }
  [pool drain];
}
