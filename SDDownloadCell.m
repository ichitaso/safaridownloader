#include "SDDownloadCell.h"

@implementation SDDownloadCell
@synthesize progressView,rightTextLabel;
-(id)initWithReuseIdentifier:(NSString*)identifier {
  if((self=[super initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:identifier])){
    UIView* contentView=self.contentView;
    [contentView addSubview:progressView=[[UIProgressView alloc] init]];
    UILabel* ref=self.detailTextLabel;
    rightTextLabel=[[UILabel alloc] init];
    rightTextLabel.textColor=ref.textColor=[UIColor grayColor];
    rightTextLabel.highlightedTextColor=ref.highlightedTextColor;
    rightTextLabel.textAlignment=UITextAlignmentRight;
    [contentView addSubview:rightTextLabel];
  }
  return self;
}
-(void)layoutSubviews {
  [super layoutSubviews];
  UILabel* ref=self.detailTextLabel;
  CGRect frame=ref.frame;
  CGFloat w=self.contentView.bounds.size.width,m=frame.origin.x;
  if(!progressView.hidden){
    UILabel* tLabel=self.textLabel;
    CGRect tFrame=tLabel.frame,pFrame=progressView.frame;
    pFrame.origin.x=m;
    pFrame.origin.y=(tFrame.origin.y+tFrame.size.height
     +frame.origin.y-pFrame.size.height)/2;
    pFrame.size.width=w-2*m;
    progressView.frame=pFrame;
    CGFloat dY=pFrame.size.height/2+3;
    tFrame.origin.y-=dY;
    tLabel.frame=tFrame;
    frame.origin.y+=dY;
    ref.frame=frame;
  }
  frame.size=[rightTextLabel.text sizeWithFont:rightTextLabel.font=ref.font];
  frame.origin.x=w-m-frame.size.width;
  rightTextLabel.frame=frame;
}
-(void)dealloc {
  [progressView release];
  [rightTextLabel release];
  [super dealloc];
}
@end
